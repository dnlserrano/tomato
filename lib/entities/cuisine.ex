defmodule Tomato.Cuisine do
  defstruct [:id, :name]
end
