defmodule Tomato.Establishment do
  defstruct [:id, :name]
end
