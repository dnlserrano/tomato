defmodule Tomato.Rating do
  defstruct [
    :aggregate_rating,
    :rating_text,
    :rating_color,
    :votes,
  ]
end
