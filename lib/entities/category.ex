defmodule Tomato.Category do
  defstruct [:id, :name]
end
