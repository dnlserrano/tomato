defmodule Tomato.City do
  defstruct [
    :id, :name, :country_id, :country_name,
    :is_state, :state_id, :state_name, :state_code
  ]
end
