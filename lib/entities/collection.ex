defmodule Tomato.Collection do
  defstruct [
    :id, :title, :url, :description,
    :image_url, :res_count, :share_url
  ]
end
